/**
 * Backend JavaScript
 * Bsexy
 *
 * Copyright (c) 2016. by Way2CU, http://way2cu.com
 * Authors: Mladen Mijatov
 */

var Caracal = Caracal || new Object();

Caracal.add_property_row = function(property_id, data, container) {
	// add two phone numbers by default
	var row = document.createElement('div');
	row.classList.add('list_item');
	container.append(row);

	// update field name
	data['text_id'] += property_id.toString();

	// create data field
	var data_field = document.createElement('input');
	with (data_field) {
		setAttribute('type', 'hidden');
		setAttribute('name', 'property_data_' + property_id);
		value = JSON.stringify(data);
	}
	row.append(data_field);

	// create columns
	var column_name = document.createElement('span');
	var column_type = document.createElement('span');
	var column_options = document.createElement('span');

	with (column_name) {
		classList.add('column');
		innerHTML = data.name[Caracal.language.current_language];
		style.width = '250px';
	}
	row.append(column_name);

	with (column_type) {
		classList.add('column');
		innerHTML = data.type;
		style.width = '60px';
	}
	row.append(column_type);

	with (column_options) {
		classList.add('column');
		classList.add('options');
	}
	row.append(column_options);

	// create options
	var option_remove = document.createElement('a');
	var option_change = document.createElement('a');
	var space = document.createTextNode(' ');

	option_change.addEventListener('click', Caracal.Shop.edit_property);
	option_remove.addEventListener('click', Caracal.Shop.delete_property)
	column_options.append(option_change);
	column_options.append(space);
	column_options.append(option_remove);

	// load language constants for options
	Caracal.language.load_text_array(null, ['delete', 'change'], function(data) {
			option_remove.innerHTML = data['delete'];
			option_change.innerHTML = data['change'];
		});
};


/**
 * Automatically add phone properties to the new item.
 */
Caracal.add_properties = function(shop_window) {
	// make sure we are working with the right window
	if (shop_window.id != 'shop_item_add')
		return true;

	// set expiration date a month from today
	var expiration_field = shop_window.ui.container.querySelector('input[name=expires]');
	if (!expiration_field)
		return true;  // window load even is called twice sometimes

	var date = new Date();
	date.setMonth(date.getMonth() + 1);

	var month = (date.getMonth() + 1).toString();
	if (month.length == 1)
		month = '0' + month;

	var day = date.getDate();
	if (day.length == 1)
		day = '0' + day;

	var value = date.getUTCFullYear() + '-' + month + '-' + day + 'T00:00';
	expiration_field.value = value;

	// set default priority
	var priority_field = shop_window.ui.container.querySelector('input[name=priority]');
	priority_field.value = '5';

	// data to add
	var data = {
			text_id: 'phone',
			name: {
				'he': 'Phone number',
				'en': 'Phone number'
				},
			type: 'text',
			value: ''
		};

	// get container
	var container = shop_window.ui.container.querySelector('div#item_properties.list_content');

	// add two phone numbers
	data2 = data;
	Caracal.add_property_row(1, data, container);
	Caracal.add_property_row(2, data2, container);
};

/**
 * Reload item list when shop items are added or changed.
 *
 * @param object shop_window
 * @return boolean
 */
Caracal.handle_shop_window_close = function(shop_window) {
	var handled_windows = ['shop_item_add', 'shop_item_change'];

	// handle only specific windows
	if (handled_windows.indexOf(shop_window.id) == -1)
		return true;

	var bsexy_window = Caracal.window_system.get_window('bsexy_items');
	if (bsexy_window)
		bsexy_window.load_content();
}

/**
 * Update item management window when filters change.
 *
 * @param object sender
 */
Caracal.update_bsexy_item_list = function(sender) {
	var items_window = Caracal.window_system.get_window('bsexy_items');
	var manufacturer = items_window.ui.container.querySelector('select[name=manufacturer]');
	var supplier = items_window.ui.container.querySelector('select[name=supplier]');
	var category = items_window.ui.container.querySelector('select[name=category]');

	// prepare data to send to server
	var data = {
			manufacturer: manufacturer.value,
			supplier: supplier.value,
			category: category.value
		};

	// save original url for later use
	if (items_window.original_url == undefined)
		items_window.original_url = items_window.url;

	// create params object for easier modification
	var url = new URL(items_window.original_url);
	var params = new URLSearchParams(url.search);

	params.set('manufacturer', manufacturer.value);
	params.set('supplier', supplier.value);
	params.set('category', category.value);
	url.search = params.toString();

	// reload window
	items_window.loadContent(url.toString());
};

/**
 * Update tags entry for before item data is saved.
 *
 * @param object window
 */
Caracal.update_tags = function(shop_window) {
	var handled_windows = ['shop_item_add', 'shop_item_change'];

	// handle only specific windows
	if (handled_windows.indexOf(shop_window.id) == -1)
		return true;

	// find elements
	var tags = shop_window.ui.container.querySelector('input[name=tags]');
	var categories = shop_window.ui.container.querySelectorAll('input[type=checkbox][name^=category_id]');
	var selected = new Array();

	for (var i=0, count=categories.length; i<count; i++) {
		var category = categories[i];

		if (!category.checked)
			continue;

		var data = category.parentNode.innerText;
		if (data)
			selected.push(data);
	}

	// update tags container
	tags.value = JSON.stringify(selected);
};


/**
 * Attach additional handler for shop categories checkbox. This
 * handler will select child categories as well when parent one
 * is checked.
 *
 * @param object shop_window
 */
Caracal.attach_category_click_handler = function(shop_window) {
	var handled_windows = ['shop_item_add', 'shop_item_change'];

	// handle only specific windows
	if (handled_windows.indexOf(shop_window.id) == -1)
		return true;

	// find all checkboxes
	var categories = shop_window.ui.container.querySelectorAll('input[type=checkbox][name^=category_id]');
	var exclude_list = ['escort', 'apartments', 'massage', 'private', 'vip'];

	for (var i=0, count=categories.length; i<count; i++)
		categories[i].addEventListener('change', function(event) {
			var category = event.target;
			var container = category.closest('tr').querySelector('div.children');
			var children = container.querySelectorAll('input[type=checkbox][name^=category_id]');
			var text_id = category.dataset.textId;

			if (exclude_list.indexOf(text_id) == -1)
				for (var i=0, count=children.length; i<count; i++)
					children[i].checked = category.checked;
		});
};


window.addEventListener('load', function() {
	Caracal.window_system.events.connect('window-content-load', Caracal.add_properties);
	Caracal.window_system.events.connect('window-content-load', Caracal.attach_category_click_handler);
	Caracal.window_system.events.connect('window-before-submit', Caracal.update_tags);
	Caracal.window_system.events.connect('window-close', Caracal.handle_shop_window_close);
});
